import java.util.Scanner;

public class ContaTerminal {
    public static void main(String[] args) {
        //TODO: Conhecer e importar a classe Scanner

        // Exibir as mensagens paras o nosso Usuário
        //Obter pela scanner os valores digitados no terminal
        //Exibir a mensagem conta criada


        Scanner sc = new Scanner(System.in);

        System.out.println("Por favor, inserir seu Nome: ");
        String nomeCliente = sc.nextLine();
        System.out.println("Por favor, inserir numero da sua Agência: ");
        String agencia = sc.nextLine();
        System.out.println("Por favor, inserir numero da sua Conta: ");
        Integer numeroConta = sc.nextInt();
        System.out.println("Por favor, inserir o saldo da Conta: ");
        Double saldo = sc.nextDouble();


        System.out.printf("Olá %s, obrigado por criar uma conta em nosso banco, " +
                "sua agência é %s, conta %s e seu saldo %.2f já está disponível para saque\"",
                nomeCliente, agencia, numeroConta, saldo);






    }
}